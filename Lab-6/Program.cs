﻿using System;
using Lab06Lib;

namespace Lab_6
{
    class Program
    {
        static void Main(string[] args)
        {
            HardDisk[] disk;
            Console.WriteLine("Enter the number of regions: ");
            int cntDisks = int.Parse(Console.ReadLine());
            disk = new HardDisk[cntDisks];

            for (int i = 0; i < cntDisks; ++i)
            {
                Console.WriteLine("Enter name of the disk: ");
                string sName = Console.ReadLine();
                Console.WriteLine("Enter the brand country: ");
                string sBrandCountry = Console.ReadLine();
                Console.WriteLine("Enter the storage capacity: ");
                string sStorageCapacity = Console.ReadLine();
                Console.WriteLine("Enter the type of interface: ");
                string sInterface = Console.ReadLine();
                Console.WriteLine("Enter buffer volume of this disk: ");
                string sBufferVolume = Console.ReadLine();
                Console.WriteLine("Enter the power of the disk(W): ");
                string sPower = Console.ReadLine();
                Console.WriteLine("Enter warranty duration: ");
                string sWarranty = Console.ReadLine();

                HardDisk theDisk = new HardDisk();
                theDisk.Name = sName;
                theDisk.BrandCountry = sBrandCountry;
                theDisk.StorageCapacity = sStorageCapacity;
                theDisk.Interface = sInterface;
                theDisk.BufferVolume = int.Parse(sBufferVolume);
                theDisk.Power = int.Parse(sPower);
                theDisk.Warranty = int.Parse(sWarranty);

                disk[i] = theDisk;
            }

            foreach(HardDisk hd in disk)
            {
                Console.WriteLine("---------------------------------------------");
                Console.WriteLine($"Name of the disk: {disk.Name}");
                Console.WriteLine($"Brand country: {disk.BrandCountry}");
                Console.WriteLine($"Storage capacity: {disk.StorageCapacity}");
                Console.WriteLine($"The type of interface: {disk.Interface}");
                Console.WriteLine($"Buffer volume of the disk: {disk.BufferVolume}");
                Console.WriteLine($"Power of the disk(W): {disk.Power}");
                Console.WriteLine($"Warranty duration: {disk.Warranty}");
                Console.WriteLine("---------------------------------------------");
            }

            Console.ReadKey();
        }
    }
}
