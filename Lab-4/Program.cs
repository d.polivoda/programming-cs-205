﻿using System;

namespace Lab_4
{
    class Program
    {
        const double StartX = 10.3;
        const double dX = 0.7;

        static double Function(double x) { return Math.Sqrt((21.02 * x * x * x + 0.03125 * x * x * x * x * x) / 1000) + 65; }

        static void Main(string[] args)
        {
            double[] arr = new double[10];
            double x = StartX;

            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                arr[i] = Function(x);
                x += dX;
            }
            Array.Sort(arr);
            Array.Reverse(arr);
            Console.WriteLine("Вiдсортованi за спаданням значення масиву: ");
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                Console.WriteLine("arr[{0:00}] = {1:0.0000}", i, arr[i]);
            }
            double aMin = arr[arr.GetUpperBound(0)];
            double aMax = arr[arr.GetLowerBound(0)];
            double aAvg = 0;
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                aAvg += arr[i];
            }
            aAvg = aAvg / arr.GetLength(0);
            Console.WriteLine("Мiнiмальне значення масиву: {0:0.0000}", aMin);
            Console.WriteLine("Максимальне значення масиву: {0:0.0000}", aMax);
            Console.WriteLine("Середнє значення масиву: {0:0.0000}", aAvg);

            int avgCounter = 0;
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i] < aAvg) avgCounter++;
                //Console.WriteLine($"arr[{i}] = {arr[i]}\t avgCounter = {avgCounter}");
            }

            Console.WriteLine($"Сума елементiв масиву, менших за середнє значення: {avgCounter}");
        }
    }
}
