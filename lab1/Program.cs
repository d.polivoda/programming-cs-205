﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введiть початкове значення Xmin: ");
            string sxMin = Console.ReadLine();
            double xMin = Double.Parse(sxMin);
            Console.Write("Введiть кiнцеве значення Xmax: ");
            string sxMax = Console.ReadLine();
            double xMax = double.Parse(sxMax);
            Console.Write("Введiть прирiст dX: ");
            string sdx = Console.ReadLine();
            double dx = double.Parse(sdx);
            double x1 = xMin;
            double x2 = 0;
            double y;
            double p = 1;
            while (x1 <= xMax)
            {
                x2 = 3 * x1;
                y = (45 * x1 * Math.Sin(x2)) + 3 * x1 * Math.Sqrt(x2 * x1);
                if (x1 > xMin && x1 < xMax)
                {
                    p *= Math.Cos(y);
                }
                Console.WriteLine("x1 = {0} x2={1} \t\t\t\t y = {2}, p = {3}", x1, x2, y, p);
                x1 += dx;

            }
            if (Math.Abs(x1 - xMax - dx) > 0.0001)
            {
                y = (45 * x1 * Math.Sin(x2)) + 3 * x2 * Math.Sqrt(x2 * x1); ;
                Console.WriteLine("x1 = {0}\t\t y = {1}", x1, x2);
            }
            Console.ReadKey();

        }
    }
}
gin