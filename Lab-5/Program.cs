﻿using System;

namespace Lab_5
{
    class Program
    {
        static void menu()
        {
            Console.WriteLine("[E] Enter disk data");
            Console.WriteLine("[D] Display data");
            Console.Write("");
        }
        static void Main(string[] args)
        {
            HardDisk disk = new HardDisk();

        Mn:
            menu();
            ConsoleKeyInfo pressedKey = Console.ReadKey();
            Console.WriteLine();
            if (pressedKey.Key == ConsoleKey.E)
            {
                Console.WriteLine();
                goto Start;
            }
            else if (pressedKey.Key == ConsoleKey.D)
            {
                Console.WriteLine();
                goto Result;
            }
            else Console.WriteLine("Smth went wrong...");

        Start:
            Console.WriteLine("Enter name of the disk: ");
            disk.Name = Console.ReadLine();
            Console.WriteLine("Enter the brand country: ");
            disk.BrandCountry = Console.ReadLine();
            Console.WriteLine("Enter the storage capacity: ");
            disk.StorageCapacity = Console.ReadLine();
            Console.WriteLine("Enter the type of interface: ");
            disk.Interface = Console.ReadLine();
            Console.WriteLine("Enter buffer volume of this disk: ");
            disk.BufferVolume = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter the power of the disk(W): ");
            disk.Power = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter warranty duration: ");
            disk.Warranty = int.Parse(Console.ReadLine());
            goto Mn;

        Result:
            Console.WriteLine("---------------------------------------------");
            Console.WriteLine($"Name of the disk: {disk.Name}");
            Console.WriteLine($"Brand country: {disk.BrandCountry}");
            Console.WriteLine($"Storage capacity: {disk.StorageCapacity}");
            Console.WriteLine($"The type of interface: {disk.Interface}");
            Console.WriteLine($"Buffer volume of the disk: {disk.BufferVolume}");
            Console.WriteLine($"Power of the disk(W): {disk.Power}");
            Console.WriteLine($"Warranty duration: {disk.Warranty}");
            Console.WriteLine("---------------------------------------------");

            goto Mn;
        }
    }
}
