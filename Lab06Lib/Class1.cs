﻿using System;

namespace Lab06Lib
{
    public class HardDisk
    {
        public string _name;
        public string _storageCapacity;
        public string _interface;
        public string _brandCountry;

        public int _bufferVolume;
        public int _power;
        public int _warranty;

        public HardDisk()
        {

        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string StorageCapacity
        {
            get { return _storageCapacity; }
            set { _storageCapacity = value; }
        }

        public string Interface
        {
            get { return _interface; }
            set { _interface = value; }
        }

        public string BrandCountry
        {
            get { return _brandCountry; }
            set { _brandCountry = value; }
        }

        public int BufferVolume
        {
            get { return _bufferVolume; }
            set { _bufferVolume = value; }
        }

        public int Power
        {
            get { return _power; }
            set { _power = value; }
        }

        public int Warranty
        {
            get { return _warranty; }
            set { _warranty = value; }
        }
    }
}
